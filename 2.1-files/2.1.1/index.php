<?php
require_once('../../back_in_menu/in_menu.html'); // Кнопка возврвта в содержание
require_once(__DIR__ . '/output.html');
$file = __DIR__ . '/data.csv';
$csv = array_map('str_getcsv', file($file));
$res = [];
for ($i = 0; $i < count($csv); $i++) {
    $res[] = explode(';', $csv[$i][0]);
}
array_walk($res, function (&$a) use ($res) {
    $a = array_combine($res[0], $a);
});

array_shift($res);

$json = json_encode($res, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
$fileJson = 'data.json';
if (!file_exists($fileJson)) {
    $fp = fopen($fileJson, 'w') or die ('чёт никак не создать файл');
    fwrite($fp, $json);
    fclose($fp);
    chmod($fileJson, 0777);
}
echo "<div class=\"container\"><p>$json</p></div>";