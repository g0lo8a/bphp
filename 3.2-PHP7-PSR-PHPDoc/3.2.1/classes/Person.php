<?php


class Person
{
    private $name;
    private $surname;
    private $patronymic;
    private $gender;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = -1;
    const GENDER_UNDEFINED = 0;

    public function __construct(string $name, string $surname, string $patronymic = null)
    {
        $this->name = $name;
        $this->surname = $surname;
        if ($patronymic != null) {
            $this->patronymic = $patronymic;
        }
        $patronymicEnding = mb_substr($patronymic, -3);
        if ($patronymicEnding == 'вич' || $patronymicEnding == 'ьич' || $patronymicEnding == 'тич' ||
            $patronymicEnding == 'глы') {
            $this->gender = self::GENDER_MALE;
        } elseif ($patronymicEnding == 'вна' || $patronymicEnding == 'чна' || $patronymicEnding == 'шна' ||
            $patronymicEnding == 'ызы') {
            $this->gender = self::GENDER_FEMALE;
        } else {
            $this->gender = self::GENDER_UNDEFINED;
        }
    }

    public function getInfo()
    {
        return "$this->surname $this->name $this->patronymic";
    }

    public function gender()
    {
        switch ($this->gender <=> 0) {
            case 1:
                return 'male';
            case 0:
                return 'undefined';
            case -1:
                return 'female';
        }
        return $this->gender;
    }

    public function genderSymbol()
    {
        switch ($this->gender <=> 0) {
            case 1:
                return "\u{2642}";
            case 0:
                return "\u{1F60E}";
            case -1:
                return "\u{2640}";
        }
        return $this->gender;
    }
}