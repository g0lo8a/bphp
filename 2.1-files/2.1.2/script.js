let fileInput = document.querySelector('.label');
let fileInputText = document.querySelector('.title');
fileInputTextContent = fileInputText.textContent;

fileInput.addEventListener('change', function(e) {
    let value = e.target.value.length > 0 ? e.target.value : fileInputTextContent;

    fileInputText.textContent = value.replace('C:\\fakepath\\', '');
});