<?php
include ('../../back_in_menu/in_menu.html');
$text = 'Hello, world!';
$image = 'img/1.jpg';
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>B_PHP-3</title>
</head>
<body>
<div class="img" style="background-image: url(<?= $image; ?>)">
    <div class="greeting">
        <h1><?= $text; ?></h1>
    </div>
</div>
</body>
</html>
