<?php
include('../../back_in_menu/in_menu.html');
$variable = 'one';

if (is_bool($variable)) {
    $type = "$variable is bool";
    $description = 'Это простейший тип. boolean выражает истинность значения.';
} elseif (is_float($variable)) {
    $type = "$variable is float";
    $description = 'Число с плавающей точкой. Используется для вещественных чисел';
} elseif (is_int($variable)) {
    $type = "$variable is int";
    $description = 'Integer - это число из множества ℤ = {..., -2, -1, 0, 1, 2, ...}.';
} elseif (is_string($variable)) {
    $type = "$variable is string";
    $description = 'Строка (тип string) - это набор символов';
} elseif (is_null($variable)) {
    $type = "$variable is null";
    $description = 'Специальное значение NULL представляет собой переменную без значения.';
};
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>B_PHP</title>
</head>
<body>
<div class="contayner">
    <div class="greeting">
        <p><?php echo $type ?></p>
        <hr>
        <p><?php echo $description ?></p>
        <hr>
        <a href="https://www.php.net/manual/ru/language.types.php">Типы в PHP. Читать на php.net</a>
    </div>
</div>
</body>
</html>
