<?php
include('../../back_in_menu/in_menu.html');
date_default_timezone_set('Europe/Moscow');

$backgroundImages = ['./img/morning.jpg', './img/day.jpg', './img/evening.jpg', './img/night.jpg'];
$greetings = ['Доброе утро!', 'Добрый день!', 'Добрый вечер!', 'Доброй ночи!'];
$wekOfDays = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];
$numberDayOfWek = (int)date('N');
$wekOfDay = $wekOfDays[$numberDayOfWek - 1];
$hours = (int)date('H');

if ($hours >= 23 || ($hours >= 0 && $hours < 6)) {
    $item = 3;
} elseif ($hours >= 6 && $hours < 11) {
    $item = 0;
} elseif ($hours >= 11 && $hours < 18) {
    $item = 1;
} else {
    $item = 2;
}

if ($numberDayOfWek === 1 || $numberDayOfWek === 2 || $numberDayOfWek === 6 || $numberDayOfWek === 7) {
    $startWorkTime = '09.00';
    if ($hours >= 9 && $hours < 18 && $numberDayOfWek !== 7) {
        $message = "Это лучший день, чтобы обратиться в Horns&Hooves!<br>Мы работаем для Вас до 18.00";
    } else {
        $message = "Сегодня (или “Завтра” или “Послезавтра”) - лучший день, чтобы обратиться в Horns&Hooves!<br>Мы работаем для Вас с $startWorkTime";
    }
} else {
    $startWorkTime = '10.00';
    if ($hours >= 9 && $hours < 18 && $numberDayOfWek !== 7) {
        $message = "Это лучший день, чтобы обратиться в Horns&Hooves!<br>Мы работаем для Вас до 18.00";
    } else {
        $message = "Сегодня (или “Завтра” или “Послезавтра”) - лучший день, чтобы обратиться в Horns&Hooves!<br>Мы работаем для Вас с $startWorkTime";
    }
}

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>B_PHP-3</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
</head>
<body>

<style>

    * {
        margin: 0;
        padding: 0;
    }

    .bg-page {
        background: #b9bbbe url("<?php echo $backgroundImages[$item] ?>") no-repeat;
        background-size: cover;
        width: 100%;
        height: 100vh;
        display: flex;
        align-items: center;
        align-content: center;
        justify-content: center;
        overflow: auto;
    }

    .container {
        background: rgba(40, 43, 68, 0.7);
        text-align: center;
        color: #fff;
        border-radius: 5px;
    }

    h1, h2, p {
        padding: 20px;
    }

</style>

<div class="bg-page">
    <div class="container">
        <h1><?php echo $greetings[$item] ?></h1>
        <hr>
        <h2> Сегодня <?php echo $wekOfDay ?></h2>
        <p><?php echo $message ?></p>
    </div>
</div>

</body>
</html>

