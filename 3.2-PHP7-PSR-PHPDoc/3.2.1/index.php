<?php
require_once 'autoload.php';

$newPerson = new Person('Иван', 'Иванов', 'Иванович')

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>bPHP - 3.2.1</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<h2>
    <span class="gender-<?php echo $newPerson->gender() ?>"><?php echo $newPerson->genderSymbol() ?></span>
    <?php echo $newPerson->getInfo() ?>
</h2>
</body>
</html>

