<?php
$jsonData = json_decode(file_get_contents(__DIR__ . '/database/files/data.json'), true);

$arrayTask = [];
foreach ($jsonData as $keys => $item) {
    foreach ($item as $key => $value) {
        if ($key == 'translator') {
            $arrayTask[$value][] = $key;
        }
    }
}

$taskCount = [];
foreach ($arrayTask as $key => $item) {
    $taskCount[$key] = count($item);
}

return $taskCount;