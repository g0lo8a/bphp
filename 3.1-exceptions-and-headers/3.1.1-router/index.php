<?php
/**
 * Доступные страницы на сайте
 *
 * @var array $availableLinks
 */
$availableLinks = include './availableLinks.php';
require_once 'autoload.php';

$test = new Router($_GET['page'], $availableLinks);

try {
    $test->isAvailablePage();
    echo "Вы находитесь на странице <b>{$_GET['page']}</b>";
} catch (Exception $e) {
    header('HTTP/1.1 400 Bad Request');
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/BPHP-3/3.1-exceptions-and-headers/3.1.1-router/error.php');
}

try {
    $test->isAvailablePage();
    echo "Вы находитесь на странице <b>{$_GET['page']}</b>";
} catch (Exception $e) {
    header('HTTP/1.1 404 Not Found');
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/BPHP-3/3.1-exceptions-and-headers/3.1.1-router/404.php');
}


