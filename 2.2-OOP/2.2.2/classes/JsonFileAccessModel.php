<?php


class JsonFileAccessModel
{
    protected $fileName;
    protected $file;

    public function __construct($fileName)
    {
        $this->fileName = $_SERVER['DOCUMENT_ROOT'] . Config::DATABASE_PATH . $fileName . '.json';
    }

    private function connect()
    {
        $this->file = fopen($this->fileName, 'r+');
    }

    private function disconnect()
    {
        fclose($this->file);
    }

    public function read()
    {
        $this->connect();
        $context = fread($this->file, filesize($this->fileName));
        $this->disconnect();
        return $context;
    }

    public function write($text)
    {
        $this->connect();
        ftruncate($this->file, 0);
        fwrite($this->file, $text);
        $this->disconnect();
        return $this->file;
    }

    public function readJson()
    {
        return json_decode($this->read());
    }

    public function writeJson($jsonObj)
    {
        $this->write(json_encode($jsonObj,
            JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
    }
}