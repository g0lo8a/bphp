<?php require_once '../../back_in_menu/in_menu.html'; ?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ERROR</title>
    <link rel="stylesheet" href="../../style_inner_pages.css">
</head>
<body>
<?php echo "<div class='container info red'><p>Слишком часто вводите пароль. Попробуйте еще раз через минуту</p></div>"; ?>
</body>
</html>
