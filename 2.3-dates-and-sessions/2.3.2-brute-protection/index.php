<?php

if (isset($_COOKIE['error1minute']) && ($_COOKIE['error1minute'] >= 3)) {
    $text = date('d.m.Y H:i:s', time()) . "\n";
    $fileName = $_COOKIE['fileName'];
    file_put_contents("$fileName.txt", $text, FILE_APPEND);
    header('Location: http://' . $_SERVER['HTTP_HOST'] .
        '/BPHP-3/2.3-dates-and-sessions/2.3.2-brute-protection/error.php');
}

require_once '../../back_in_menu/in_menu.html';
require_once 'form.html';
$users = [
    'admin' => 'ac9689e2272427085e35b9d3e3e8bed88cb3434828b43b86fc0596cad4c6e270',
    'randomUser' => 'baae90bd064867ab28f034d6ed40ef14684012e4c0567181eaee3494a2358695',
    'janitor' => '7f64d06cb9d3835a8909feb50c943b72bcbd6f54a2a61ae5729ae22b18ffc8bd'
];

// login: admin: password: admin1234
// login: randomUser: password: somePassword
// janitor: admin: password: nimbus2000

$counter = 1;
$counterMinute = 1;
$ok = "<div class='container message green'><p>o'k</p></div>";
$error = "<div class='container message red'><p>error</p></div>";

function clean($value = "")
{
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);

    return $value;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST)) {
        if (isset($_POST['login']) && isset($_POST['password'])) {
            $userLogin = clean($_POST['login']);
            $userPassword = clean($_POST['password']);

            if (array_key_exists($userLogin, $users)) {

                if ($users[$userLogin] === hash('sha256',$userPassword)) {
                    echo $ok;
                    $counter = 0;
                    setcookie('error5seconds', $counter, time() - 1);
                    setcookie('error1minute', $counter, time() - 1);
                } else {
                    if (isset($_COOKIE['error5seconds'])) {
                        $counter = $_COOKIE['error5seconds'];
                        $counter++;
                    }
                    if ($counter == 2) {
                        setcookie('fileName', $userLogin, strtotime('+1 minute'));
                        file_put_contents("$userLogin.txt", $text, FILE_APPEND);
                        header('Refresh:0; url=error.php');
                    }

                    if (isset($_COOKIE['error1minute'])) {
                        $counterMinute = $_COOKIE['error1minute'];
                        $counterMinute++;
                    }
                    if ($counterMinute >= 3) {
                        setcookie('fileName', $userLogin, strtotime('+1 minute'));
                        file_put_contents("$userLogin.txt", $text, FILE_APPEND);
                        header('Refresh:0; url=error.php');
                    }

                    echo $error;
                    setcookie('error5seconds', $counter, strtotime('+5 seconds'));
                    setcookie('error1minute', $counterMinute, strtotime('+1 minute'));
                }
            } else {
                echo $error;
            }
        }
    }
}