<?php

/**
 * @param string $serviceName
 * @param null $serviceValue
 * @return string
 */
function purchaseOrderConcat(string $serviceName, $serviceValue = null)
{
    if (!$serviceValue) {
        return "<div class=\"order322-line\">$serviceName</div>";
    }
    return "<div class=\"order322-line\"><div>$serviceName</div><div>$serviceValue ₽</div></div>";
}

/**
 * @param $menu
 * @param $request
 * @return string
 * @throws Exception
 */
function countTheBill($menu, $request)
{
    $numberOfBill = random_int(1000, 9999);
    $purchaseOrder = "<div class=\"order322-line order322-title\">Счёт №$numberOfBill</div>";
    $total = 0;
    foreach ($menu as $dishOfMenu) {
        $numberOfServings = $request[$dishOfMenu->id];
        if ($numberOfServings > 0) {
            $priceOfValue = $numberOfServings * $dishOfMenu->price;
            $priceOfValueFormat = number_format($dishOfMenu->price * $numberOfServings, 2);
            $price = number_format($dishOfMenu->price, 2);
            $purchaseOrder .= purchaseOrderConcat($dishOfMenu->name, "$numberOfServings * $price ₽ = $priceOfValueFormat ");
            $total += $priceOfValue;
        }
    }
    $service = (int)$request['service'];
    if ($total > 0) {
        if ($service === 2) {
            $discount = 0.1;
            $servicePrice = number_format($total * $discount, 2);
            $purchaseOrder .= purchaseOrderConcat('Скидка 10% (самовывоз)', $servicePrice);
            $total = $total - (float)$servicePrice;
        } elseif ($service === 4) {
            $tip = 0.1;
            $servicePrice = number_format($total * $tip, 2);
            $purchaseOrder .= purchaseOrderConcat('Чаевые 10%', $servicePrice);
            $total = $total + (float)$servicePrice;
        } elseif ($service === 1) {
            $delivery = 200;
            $purchaseOrder .= purchaseOrderConcat('Доставка', $delivery);
            $total = $total + $delivery;
        }
    } else {
        $purchaseOrder .= purchaseOrderConcat('Ничего не заказано');
    }
    $total = number_format($total, 2);

    $purchaseOrder .= "<div class=\"order322-total\"><div>Итого: $total ₽</div></div>";
    return $purchaseOrder;
}