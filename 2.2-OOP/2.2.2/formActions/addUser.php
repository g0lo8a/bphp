<?php

require_once __DIR__ . '/../autoload.php';
require_once __DIR__ . '/../config/SystemConfig.php';


$count = 0;
$warningLogin = 'Поле Логин не может содержать символы @ / * ? , ; : .';
$warningPassword = 'Длина пароля должна быть минимум 8 символов';
$warningEmail = 'Почта должна быть формата почта@домен.доменнаязона';
$warningRate = 'Введено не число';

/**
 * @param string $value
 * @return string
 */
function clean($value = "")
{
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);

    return $value;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST)) {
        if ((isset($_POST['name'])) && (isset($_POST['password'])) && (isset($_POST['email'])) && (isset($_POST['rate']))) {
            $name = clean($_POST['name']);
            $password = clean($_POST['password']);
            $email = clean($_POST['email']);
            $rate = clean($_POST['rate']);
            if (!empty($name) && !empty($password) && !empty($email) && !empty($rate)) {
                if (preg_match('/\W/', $name)) {
                    echo "<div class=\"container message\"><p>$warningLogin</p></div>";
                } else {
                    ++$count;
                }
                if (strlen($password) < 8) {
                    echo "<div class=\"container message\"><p>$warningPassword</p></div>";
                } else {
                    ++$count;
                }
                if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $email)) {
                    echo "<div class=\"container message\"><p>$warningEmail</p></div>";
                } else {
                    ++$count;
                }
                if (!preg_match('/^\d+$/', $rate)) {
                    echo "<div class=\"container message\"><p>$warningRate</p></div>";
                } else {
                    ++$count;
                }
            }
        }
    }
}
if ($count == 4) {
    $testUser = new User();
    $testUser->addUserFromForm();
    var_dump($testUser);
}
header('HTTP/1.1 200 OK');
header('Location: http://' . $_SERVER['HTTP_HOST'] . '/BPHP-3/2.2-OOP/2.2.2');