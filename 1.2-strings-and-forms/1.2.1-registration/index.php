<?php
require_once('../../back_in_menu/in_menu.html'); // Кнопка возврвта в содержание
require_once('form.html');

$count = 0;
$codeWord = 'nd82jaake';
$warningLogin = 'Поле Логин не может содержать символы @ / * ? , ; : .';
$warningPassword = 'Длина пароля должна быть минимум 8 символов';
$warningEmail = 'Почта должна быть формата почта@домен.доменнаязона';
$warningName = 'В Имени должно быть больше символов';
$warningLastName = 'В Фамилии должно быть больше символов';
$warningMiddleName = 'В Отчестве должно быть больше символов';
$warningCodeWord = 'Не правильное кодовое слово';

function clean($value = "")
{
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);

    return $value;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST)) {
        $login = clean($_POST['login']);
        $password = clean($_POST['password']);
        $email = clean($_POST['email']);
        $firstName = clean($_POST['firstName']);
        $lastName = clean($_POST['lastName']);
        $middleName = clean($_POST['middleName']);
        $code = clean($_POST['code']);

        if (!empty($login) && !empty($password) && !empty($email)) {
            if (preg_match('/\W/', $login)) {
                echo "<div class=\"container message\"><p>$warningLogin</p></div>";
            } else {
                ++$count;
            }
            if (strlen($password) < 8) {
                echo "<div class=\"container message\"><p>$warningPassword</p></div>";
            } else {
                ++$count;
            }
            if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $email)) {
                echo "<div class=\"container message\"><p>$warningEmail</p></div>";
            } else {
                ++$count;
            }
            if (empty($firstName)) {
                echo "<div class=\"container message\"><p>$warningName</p></div>";
            } else {
                ++$count;
            }
            if (empty($lastName)) {
                echo "<div class=\"container message\"><p>$warningLastName</p></div>";
            } else {
                ++$count;
            }
            if (empty($middleName)) {
                echo "<div class=\"container message\"><p>$warningMiddleName</p></div>";
            } else {
                ++$count;
            }
            if ($code !== $codeWord) {
                echo "<div class=\"container message\"><p>$warningCodeWord</p></div>";
            } else {
                ++$count;
            }
        } elseif (empty($login) || empty($password)) {
            echo "<div class=\"container message\"><p>Одно или несколько полей не заполнены</p></div>";
        }
    }
}

if ($count == 7) {
    echo "<div class=\"container message\"><p>Успех</p></div>";
}