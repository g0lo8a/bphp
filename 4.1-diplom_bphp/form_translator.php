<?php
require_once 'functions.php';
include 'header.php'
?>
    <title><?php echo $_SESSION['user']['userName'] ?></title>
</head>

<body>
<?php
require_once 'nav.php';

$id = "<input type='hidden' name='id' value='{$_POST['id']}'>";
$translator = "<input type='hidden' name='translator' value='{$_POST['translator']}'>";
$client = "<input type='hidden' name='client' value='{$_POST['client']}'>";
$original = "<input type='hidden' name='original' value='{$_POST['original']}'>";
$datePost = "<input type='hidden' name='date' value='{$_POST['date']}'>";

$translatePost = explode(' ', clean($_POST['translate']));

?>
<div class="container__wrapper">
    <div class="form__container">
        <div class="button__wrapper close">
            <a class="link" href="javascript:history.back()">Закрыть</a>
        </div>
        <?php ?>
        <form action="action.php" method="post">
            <div class="container__wrapper space">
                <div>
                    <p>Язык оригинала</p>
                    <p><?php echo $_POST['original'] ?></p>
                </div>
                <div>
                    <p>Языки перевода</p>
                    <p><?php echo $_POST['translate'] ?></p>
                </div>
                <div>
                    <p>Крайний срок</p>
                    <p><?php echo $_POST['date'] ?></p>
                </div>
            </div>
            <?php
            echo $id . $translator . $client . $original;

            foreach ($translatePost as $item) {
                echo "<input type='hidden' name='translate[]' value='$item'>";
            }
            ?>

            <label for="i21"></label>
            <textarea id="i21" class="default" name="text" readonly><?php echo $_POST['text'] ?></textarea>
            <?php
            $translateArray = explode(' ', clean($_POST['translate']));
            foreach ($translateArray as $value) {
                $textareaValue = isset($_POST["text_translate"]) ? $_POST["text_translate"] : null;
                echo "<label for='i20' class='label__textarea'>$value</label>
            <textarea id='i20' class='translate' name='text_translate[$value]'>{$textareaValue[$value]}</textarea>";
            }
            echo $datePost;
            ?>
            <div class="footer">
                <div class="footer__elem">
                    <button class="button_done send" type="submit" name="status" value="done_translator">Готово</button>
                </div>
                <div class="footer__elem">
                    <button class="button_save send" type="submit" name="status" value="save_translator">Сохранить
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include 'footer.php' ?>