<?php


class Router
{
    public $availableLinks;
    public $page;

    public function __construct($get, $link)
    {
        $this->page = $get;
        $this->availableLinks = $link;
    }

    public function isAvailablePage()
    {
        if (!$_GET || !in_array($this->page, $_GET)) {
            throw new Exception('400 Bad Request');
        }

        if (!in_array($_GET[$this->page], $this->availableLinks)) {
            throw new Exception('404 Not Found');
        }
        var_dump($this->page);
        return true;
    }
}