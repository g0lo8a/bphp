<?php
require_once 'functions.php';
require_once 'counter_tasks.php';
$json = json_decode(file_get_contents(__DIR__ . '/database/users.json'), true);
$id = isset($_POST['id']) ? $_POST['id'] : uniqid();
$date = isset($_POST['date']) ? $_POST['date'] : null;
if (isset($_POST['delete'])) {
    require_once 'action_delete_task.php';
    header('Location: task_list.php?filterParam=new');
}
include 'header.php'
?>
    <title><?php echo $_SESSION['user']['userName'] ?></title>
</head>

<body>

<?php require_once 'nav.php'; ?>

<div class="container__wrapper">
    <div class="form__container">
        <div class="button__wrapper close">
            <a class="link" href="javascript:history.back()">Закрыть</a>
        </div>
        <?php ?>
        <form action="action.php" method="post">
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <div class="container__wrapper space">
                <div class="head__elem drop_list">
                    <label for="i01_input">Исполнитель:</label>
                    <select class="select" id="i01_input" name="translator">;
                        <option value="Не назначать">Не назначать</option>
                        <?php
                        /**
                         * получаю имена исполнителей и вывожу в селект
                         */

                        foreach ($json as $keys => $value) {
                            if (!$value['isManager']) {
                                foreach ($value as $key => $val) {
                                    if ($key == 'userName') {
                                        if (isset($_POST['translator']) && $val == $_POST['translator']) {
                                            $selected = 'selected';
                                        } else {
                                            $selected = null;
                                        }
                                        echo "<option value='$val' name='$val' $selected>$val  (заданий-{$taskCount[$val]})</option>";
                                    }
                                }
                            }
                        }
                        ?>

                    </select>
                </div>

                <div class="head__elem">
                    <label for="i02_input">Клиент:</label>
                    <input class="input-client" id="i02_input" type="text"
                           placeholder="ООО &quot;Фёдор и страховка&quot;" name="client"
                           value="<?php if (isset($_POST['client'])) echo $_POST['client']; ?>">
                </div>

            </div>

            <div class="fieldset">
                <fieldset>

                    <legend>Язык оригинала</legend>

                    <?php
                    $originalValues = ['Русский', 'Английский', 'Немецкий', 'Французский', 'Итальянский', 'Испанский'];
                    foreach ($originalValues as $value) {
                        $checked = isset($_POST['original']) && $_POST['original'] == $value ? 'checked' : null;
                        echo "<label class='label-radio'>$value<input class='radio' type='radio' value='$value' name='original' $checked></label>";
                    }
                    ?>

                </fieldset>

                <fieldset>

                    <legend>Языки перевода</legend>

                    <?php
                    $translateLabels = ['Русский', 'Английский', 'Немецкий', 'Французский', 'Итальянский', 'Испанский'];
                    $translateValues = ['RU', 'EN', 'DE', 'FR', 'IT', 'ESP'];
                    for ($i = 0; $i < count($translateLabels); $i++) {
                        $translateLabel = $translateLabels[$i];
                        $translateValue = $translateValues[$i];

                        echo "<label class='label-checkbox'>$translateLabel<input class='checkbox' type='checkbox' value='$translateValue' name='translate[]' " .
                            isChecked($translateValue) . "></label>";
                    }
                    ?>

                </fieldset>
            </div>
            <label>
                <textarea class="default" cols="" rows="" name="text"><?php
                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        if (!empty($_POST)) {
                            echo $_POST['text'];
                        }
                    } ?></textarea>
            </label>
            <?php

            foreach ($_POST as $key => $value) {
                if (isset($_POST['text_translate'])) {
                    foreach ($_POST['text_translate'] as $item => $val) {
                        if ($key == 'translate') {
                            echo "<label for='i20' class='label__textarea'>$item</label>
            <textarea id='i20' class='translate' name='text_translate[$value]'>{$val}</textarea>";
                        }
                    }
                }
            }
            ?>
            <div class="footer">
                <div class="footer__elem">
                    <button class="button_done send" type="submit" name="status" value="done">Готово</button>
                </div>
                <div class="footer__elem">
                    <button class="button_finalize send" type="submit" name="status" value="finalize">Доработать
                    </button>
                </div>
                <div class="footer__elem last">
                    <button class="button_save send" type="submit" name="status" value="save_manager">Сохранить</button>
                </div>
                <div class="footer__elem">
                    <label for="date">Крайний срок</label>
                    <input class="select-date" name="date" id="date" type="date" value="<?php echo $date ?>">
                </div>
            </div>
        </form>
    </div>
</div>
<?php include 'footer.php' ?>