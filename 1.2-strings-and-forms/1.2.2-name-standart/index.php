<?php
require_once('../../back_in_menu/in_menu.html'); // Кнопка возврвта в содержание
require('form.html');

function clean($value = "")
{
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);

    return $value;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') { // проверил метод
    if (!empty($_POST)) { // проверил есть что-то
        $firstName = clean($_POST['firstName']);
        $lastName = clean($_POST['lastName']);
        $middleName = clean($_POST['middleName']);
        if (!empty($firstName) && !empty($lastName) && !empty($middleName)) { // проверил всё ли на месте...

            $firstLetterOfFirstName = mb_strtoupper(mb_substr($firstName, 0, 1));
            $firstLetterOfLastName = mb_strtoupper(mb_substr($lastName, 0, 1));
            $firstLetterOfMiddleName = mb_strtoupper(mb_substr($middleName, 0, 1));
            $fio = "$firstLetterOfLastName$firstLetterOfFirstName$firstLetterOfMiddleName";

            $lastName = $firstLetterOfLastName . mb_substr($lastName, 1);
            $firstName = $firstLetterOfFirstName . mb_substr($firstName, 1);
            $middleName = $firstLetterOfMiddleName . mb_substr($middleName, 1);
            $fullName = "$lastName $firstName $middleName";

            $surnameAndInitials = "$lastName $firstLetterOfFirstName.$firstLetterOfMiddleName";


            echo "<div class=\"container message\"><p>Полное имя: '$fullName'</p></div>";
            echo "<div class=\"container message\"><p>Фамилия и инициалы: '$surnameAndInitials'</p></div>";
            echo "<div class=\"container message\"><p>Аббревиатура: '$fio'</p></div>";
        }
    }
}