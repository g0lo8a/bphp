<?php

$fileName = $_SERVER['DOCUMENT_ROOT'] . '/BPHP-3/4.1-diplom_bphp/database/files/' . 'data' . '.json';
// открываю файл
$file = file_get_contents($fileName);
// декодирую в массив
$taskList = json_decode($file, true);

// очищаю переменную $file
unset($file);
// записываю новый объект в массив по id(если такого id нет – добавит, а если есть – перезапишет его)
$id = $_POST['id'];
$taskList[$id] = json_decode(json_encode($_POST,
    JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT), true);
// перезаписываю файл уже с новым обЪектом
file_put_contents($fileName, json_encode($taskList,
    JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
unset($taskList);

header('Location: task_list.php?filterParam=new');