<?php
require_once __DIR__ . '/functions.php';
if (!isGuest()) {
    header('Location: index.php');
}
$errors = [];
if (!empty($_POST)) {
    if (login($_POST['login'], $_POST['password'])) {
        header('Location: index.php');
    } else {
        $errors[] = '<h2>Неверный логин и пароль</h2>';
    }
}
include 'header.php'
?>
    <title>Login</title>
</head>
<body>
<header><h1>Информационная система «Бюро переводов»</h1></header>
<div class="container__wrapper form__inner">
    <div class="container__form-inner">
        <h1 class="login">Авторизация</h1>
        <h2>
            <?php
            foreach ($errors as $error) {
                echo $error;
            }
            ?>
        </h2>
        <form method="post">
            <div>
                <label class="inner" for="login">Логин</label>
                <input class="input-inner" type="text" id="login" name="login" placeholder="Логин">
            </div>
            <div>
                <label class="inner" for="password">Пароль</label>
                <input class="input-inner" type="password" id="password" name="password" placeholder="Пароль">
            </div>
            <div>
                <button type="submit" class="send inner">Войти</button>
            </div>
        </form>
    </div>
</div>
<?php include 'footer.php' ?>