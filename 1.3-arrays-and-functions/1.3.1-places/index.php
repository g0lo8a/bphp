<?php
require_once('../../back_in_menu/in_menu.html'); // Кнопка возврвта в содержание

function generatingMapOfSeats($numberOfRows, $numberOfSeats, $numberOfAvailableSeats)
{
    if ($numberOfRows * $numberOfSeats > $numberOfAvailableSeats) {
        $result = false;
    } else {
        $result = [];
        for ($i = 1; $i <= $numberOfRows; $i++) {
            for ($j = 1; $j <= $numberOfSeats; $j++) {
                $result[$i][$j] = false;
            }
        }
    }
    return $result;
}

function reserve(&$map, $row, $place)
{
    if ($map[$row][$place] === false) {
        $map[$row][$place] = true;
        return true;
    } else{
        return false;
    }
}

$chairs = 50;
$map = generatingMapOfSeats(5, 8, $chairs);
$requireRow = 4;
$requirePlace = 4;

$reserved = reserve($map, $requireRow, $requirePlace);
logReserve($requireRow, $requirePlace, $reserved);

$reserved = reserve($map, $requireRow, $requirePlace);
logReserve($requireRow, $requirePlace, $reserved);
function logReserve($row, $place, $result)
{
    if ($result) {
        echo "<div class=\"container message\"><p>Ваше место забронировано! Ряд $row, место $place</p></div>";

    } else {
        echo "<div class=\"container message\"><p>Что-то пошло не так=( Бронь не удалась</p></div>";
    }
}

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Задача 1. Посадочные места</title>
</head>
<body>

</body>
</html>
