<?php
require_once('../../back_in_menu/in_menu.html'); // Кнопка возврвта в содержание
require('form.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST') { // проверил метод
    if (!empty($_POST)) { // проверил есть что-то

        $input__http = trim($_POST['input__http']);
        $input__date = trim($_POST['input__date']);
        $input__price = trim($_POST['input__price']);

        if (!empty($input__http)) {
            if (substr($input__http, 0, 5) === 'https') {
                echo "<div class=\"container message\"><p>substr(<span class=\"green\">Да</span>);</p></div>";
            } else {
                echo "<div class=\"container message\"><p>substr(<span class=\"red\">Нет</span>);</p></div>";
            }
            if (strpos($input__http, ':') === 5) {
                echo "<div class=\"container message\"><p>strpos(<span class=\"green\">Да</span>);</p></div>";
            } else {
                echo "<div class=\"container message\"><p>strpos(<span class=\"red\">Нет</span>);</p></div>";
            }
        }
        if (!empty($input__date)) {
            $input__date = str_replace('-', '.', $input__date);
            $day = substr($input__date, 0, 3);
            $monthYear = substr_replace($input__date, '', 0, 3);
            $input__date = substr_replace($monthYear, $day, 3, 0);
            echo "<div class=\"container message\"><p>Дата OUTPUT: $input__date</p></div>";
        }
        if (!empty($input__price)) {
            $input__price = substr_replace($input__price, ' ', -3, 0);
            $input__price = substr_replace($input__price, ' ', -7, 0);
            echo "<div class=\"container message\"><p>Дата OUTPUT: $input__price</p></div>";
        }
    }
}