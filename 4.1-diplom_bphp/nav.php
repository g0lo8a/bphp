<?php require_once 'functions.php'; ?>
<div class="stabiliser"></div>
<div class="menu__wrapper">
    <div class="menu__container">
        <div class="menu__item">
            <a class="nav-link link" href="task_list.php?filterParam=new">Новые</a>
        </div>
        <div class="menu__item">
            <a class="nav-link link" href="task_list.php?filterParam=check">На проверке</a>
        </div>
        <div class="menu__item">
            <a class="nav-link link" href="task_list.php?filterParam=finalize">На доработке</a>
        </div>
        <div class="menu__item">
            <a class="nav-link link" href="task_list.php?filterParam=done">Готово</a>
        </div>
        <?php if (isManager()): ?>
            <div class="menu__item">
                <a class="nav-link link" href="form_manager.php">Создать новое</a>
            </div>
        <?php endif ?>
        <div class="menu__item"><a class="nav-link link" href="logout.php">Выход</a></div>
    </div>
</div>