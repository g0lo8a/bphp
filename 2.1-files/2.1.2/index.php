<?php
require_once('../../back_in_menu/in_menu.html'); // Кнопка возврвта в содержание
require('form.html');

$uploadsDir = 'uploads';

if (!empty($_FILES) && $_FILES['picture']['error'] == 0) { // Проверяем, загрузил ли пользователь файл
    $name = $_FILES['picture']['name'];
    $tmpName = $_FILES['picture']['tmp_name'];
    if (is_uploaded_file($tmpName)) {
        $pathParts = pathinfo($_FILES['picture']['name']);
        if ($pathParts['extension'] != 'php') {
            move_uploaded_file($tmpName, "$uploadsDir/$name"); // Перемещаем файл в желаемую директорию (папка uploads)
            chmod("$uploadsDir/$name", 0777);
            echo "<div class=\"container\"><p>File Uploaded</p></div>"; // Оповещаем пользователя об успешной загрузке файла
            /*echo "<div class=\"container\"><img src=\"$uploadsDir/$name\" alt=\"Картинка\" width=\"250\"></div>";*/
        }
    }
} else {
    echo "<div class=\"container\"><p>No File Uploaded</p></div>"; // Оповещаем пользователя о том, что файл не был загружен
}
?>
<div class="container gallery">
    <?php
$files = scandir($uploadsDir);
foreach ($files as $file) {
    if ($file === '.' || $file === '..') {
        continue;
    }
    if (substr($file, -4) === '.jpg' || substr($file, -4) === '.png' || substr($file, -4) === '.gif') {
        echo "<div class=\"img\"><img src=\"$uploadsDir/$file\" alt=\"pic\"></div>";
    }
}
?>
</div>
