<?php
$fileName = $_SERVER['DOCUMENT_ROOT'] . '/BPHP-3/4.1-diplom_bphp/database/files/' . 'data' . '.json';

// открываю файл
$file = file_get_contents($fileName);

// декодирую в массив
$taskList = json_decode($file, true);

// Найти в массиве Переменную
foreach ($taskList as $key => $value) {
    if (in_array($id, $value)) {

// после обнаружения удалить
        unset($taskList[$key]);
    }
}

// очищаю переменную $file
unset($file);

// перезаписываю файл уже с новым обЪектом
file_put_contents($fileName, json_encode($taskList,
    JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
unset($taskList);
