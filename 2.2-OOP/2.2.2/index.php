<?php

require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/config/SystemConfig.php';

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../style_inner_pages.css">
</head>
<body>
<?php require_once('../../back_in_menu/in_menu.html'); // Кнопка возврвта в содержание ?>
<div class="container info"><p>Создать пользователя</p></div>
<div class="container">
    <form action="formActions/addUser.php" method="post">
        <div>
            <label for="name">Имя</label>
            <input type="text" id="name" name="name" placeholder="Имя" required>
        </div>
        <div>
            <label for="password">Пароль</label>
            <input type="password" id="password" name="password" placeholder="Пароль" required>
        </div>
        <div>
            <label for="email">Почта</label>
            <input type="email" id="email" name="email" placeholder="Почта" required>
        </div>
        <div>
            <label for="rate">Пароль</label>
            <input type="text" id="rate" name="rate" placeholder="Рейтинг" required>
        </div>
        <div>
            <button type="submit" class="send">Добавить пользователя</button>
        </div>
    </form>
</div>
<div class="container">
    <?php $test = new Users();
    $test->displaySortedList();
    ?>
</div>
</body>
</html>
