<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>B_PHP</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<main>
    <div class="main__container">
        <h1 class="page__title">Домашние задания по курсу «Основы PHP»</h1>
        <div class="avatar__container">
            <img src="avatar.jpg" alt="" class="avatar">
        </div>
        <nav class="main__navigation">
            <h2>Блок 1. Базовый синтаксис. PHP в HTML и HTML средствами PHP</h2>
            <hr>
            <h3>Занятие 1.1. Знакомство с PHP</h3>
            <ul>
                <li><a href="1.1-intro-and-branching/1.1.0">Задание 1.1.0</a></li>
                <li><a href="1.1-intro-and-branching/1.1.1">Задание 1.1.1</a></li>
                <li><a href="1.1-intro-and-branching/1.1.2">Задание 1.1.2</a></li>
            </ul>
            <h3>Занятие 1.2. Формы и строки</h3>
            <ul>
                <li><a href="1.2-strings-and-forms/1.2.1-registration">Задание 1.2.1</a></li>
                <li><a href="1.2-strings-and-forms/1.2.2-name-standart">Задание 1.2.2</a></li>
                <li><a href="1.2-strings-and-forms/1.2.3-additional">Задание 1.2.3</a></li>
            </ul>
            <h3>Занятие 1.3. Массивы и циклы</h3>
            <ul>
                <li><a href="1.3-arrays-and-functions/1.3.1-places">Задание 1.3.1</a></li>
                <li><a href="1.3-arrays-and-functions/1.3.2-search">Задание 1.3.2</a></li>
            </ul>
            <hr>
            <h2>Блок 2. Работа с файлами</h2>
            <hr>
            <h3>Занятие 2.1. Файлы</h3>
            <ul>
                <li><a href="2.1-files/2.1.1">Задание 2.1.1</a></li>
                <li><a href="2.1-files/2.1.2">Задание 2.1.2</a></li>
            </ul>
            <h3>Занятие 2.2. Введение в ООП</h3>
            <ul>
                <li><a href="2.2-OOP/2.2.1">Задание 2.2.1</a></li>
                <li><a href="2.2-OOP/2.2.2">Задание 2.2.2</a></li>
            </ul>
            <h3>Занятие 12.3. Даты. Сессии</h3>
            <ul>
                <li><a href="2.3-dates-and-sessions/2.3.1-your-tube">Задание 2.3.1</a></li>
                <li><a href="2.3-dates-and-sessions/2.3.2-brute-protection">Задание 2.3.2</a></li>
                <li><a href="2.3-dates-and-sessions/2.3.3-additional">Задание 2.3.3</a></li>
            </ul>
            <hr>
            <h2>Блок 3. PHP, как средство профессиональной разработки</h2>
            <hr>
            <h3>Занятие 3.1. Работа с исключениями. Заголовки</h3>
            <ul>
                <li><a href="3.1-exceptions-and-headers/3.1.1-router">Задание 3.1.1</a></li>
            </ul>
            <h3>Занятие 3.2. PHP7. PSR-*. PHPDOC</h3>
            <ul>
                <li><a href="3.2-PHP7-PSR-PHPDoc/3.2.1">Задание 3.2.1</a></li>
                <li><a href="3.2-PHP7-PSR-PHPDoc/3.2.2">Задание 3.2.2</a></li>
            </ul>
            <hr>
            <h2>Блок 4. Дипломный проект</h2>
            <hr>
            <h3>Дипломный проект по курсу Основы PHP</h3>
            <ul>
                <li><a href="4.1-diplom_bphp">«Разработка информационной системы Бюро
                        переводов»</a></li>
            </ul>
        </nav>
    </div>
</main>
</body>
</html>

