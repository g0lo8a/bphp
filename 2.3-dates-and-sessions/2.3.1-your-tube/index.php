<?php
/**
 * Функция получает текущее количество просмотров на видео
 *
 * @return int
 */
function getViews()
{
    $views = include 'views.php';
    return (int)$views;
}

/**
 * Функция увеличивает количество просмотров на 1
 *
 * @param int $views
 */
function incrementViews($views)
{
    $views++;
    $data = "<?php \r\nreturn {$views};";
    file_put_contents('views.php', $data);
}

/**
 * Функция проверяет, нужно ли увеличивать число просмотров
 *
 * @return bool
 */
function shouldBeIncremented():bool
{
    if (!isset($_COOKIE['views'])) {
        setcookie('views', 'yes', time() + 300);
        return true;
    } else {
        setcookie('views', 'yes', time() + 300);
        return false;
    }
}

if (shouldBeIncremented()) {
    incrementViews(getViews());
}

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Your Tube</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php //require_once '../../back_in_menu/in_menu.html'; ?>
<header>
    <h1>YOUR TUBE</h1>
</header>

<div class="container">
    <div class="main__container">
        <div class="main__video">
            <p>Очень интересное видео</p>
        </div>
        <div>
            <p><b>Просмотров: <?php echo getViews() ?> </b></p>
        </div>
    </div>
    <div class="other-video__container">
        <div class="other-video">
            <p>Очень интересное видео</p>
        </div>
        <div class="other-video">
            <p>Очень интересное видео</p>
        </div>
        <div class="other-video">
            <p>Очень интересное видео</p>
        </div>
        <div class="other-video">
            <p>Очень интересное видео</p>
        </div>
    </div>
</div>
</body>
</html>
