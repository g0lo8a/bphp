<?php
require_once 'functions.php';
if (isGuest()) {
    header('Location: login.php');
    die;
}

include 'header.php'
?>
    <title><?php echo $_SESSION['user']['userName'] ?></title>
</head>
<body>
<?php require_once 'nav.php' ?>
<div class="container__wrapper form__inner">
    <div class="container__form-inner">
        <h1>Привет <?php echo $_SESSION['user']['userName'] ?>!</h1>
    </div>
</div>
<?php include 'footer.php' ?>