<?php
require_once 'functions.php';
include 'header.php'
?>
    <title><?php echo $_SESSION['user']['userName'] ?></title>
</head>
<body>
<?php require_once 'nav.php'; ?>
<div class="wrap">
<div class="container__wrapper">
    <div class="form__container">
        <?php
        if (isManager()) {
            $action = "form_manager.php";
        } elseif (!isManager()) {
            $action = 'form_translator.php';
        }
        $array = json_decode(file_get_contents('database/files/data.json'), true);
        foreach ($array as $keys) {
            $translate = null;
            $text_translate = null;
            foreach ($keys as $key => $value) {

                if ($key == 'text') {
                    if (isset($keys['translate'])) {
                        foreach ($keys['translate'] as $trans) {
                            $translate .= " $trans";
                        }
                    }
                    $originalLanguage = isset($keys['original']) ? $keys['original'] : null;
                    $id = "<input type='hidden' name='id' value='{$keys['id']}'>";
                    $translator = "<input type='hidden' name='translator' value='{$keys['translator']}'>";
                    $client = "<input type='hidden' name='client' value='{$keys['client']}'>";
                    $original = "<input type='hidden' name='original' value='$originalLanguage'>";
                    $datePost = "<input type='hidden' name='date' value='{$keys['date']}'>";
                    $content =
                        "<div class='content__wrapper'><textarea name='text' class='translate' readonly>{$keys['text']}</textarea></div>";
                    $date = '<p>' . date('d/m/Y', strtotime($keys['date'])) . '</p>';
                    $translatePost = "<input name='translate' type='hidden' value='$translate'>";

                    $echoManagerTaskList =
                        $content . '<div class="redact"><button type="submit" class="link">Открыть</button>' .
                        $date . '<div><button type="submit" class="link" name="delete">Удалить</button><p>' .
                        $translate . '</p></div></div>';

                    $echoTranslatorTaskList =
                        $content . '<div class="redact"><button type="submit" class="link">Открыть</button>' .
                        $date . '<p>' . $translate . '</p></div>';

                    echo "<form action='$action' method='post'><div class='task-list__item'>$id $translator $client $original $datePost $translatePost";

                    if (isset($keys['text_translate'])) {
                        foreach ($keys['text_translate'] as $i => $j) {
                            echo "<input type='hidden' name='text_translate[$i]' value='$j'>";
                        }
                    }

                    if (!empty($_GET)) {
                        if ($_GET['filterParam'] == 'new') {
                            if (isManager() &&
                                ($keys['status'] == 'save_translator' || $keys['status'] == 'save_manager')) {
                                echo $echoManagerTaskList;
                            } elseif ($keys['translator'] == $_SESSION['user']['userName'] &&
                                ($keys['status'] == 'save_translator' || $keys['status'] == 'save_manager')) {
                                echo $echoTranslatorTaskList;
                            }
                        } elseif ($_GET['filterParam'] == 'check') {
                            if (isManager() && $keys['status'] == 'done_translator') {
                                echo $echoManagerTaskList;
                            } elseif ($keys['translator'] == $_SESSION['user']['userName'] &&
                                $keys['status'] == 'done_translator') {
                                echo $echoTranslatorTaskList;
                            }
                        } elseif ($_GET['filterParam'] == 'finalize') {
                            if (isManager() && $keys['status'] == 'finalize') {
                                echo $echoManagerTaskList;
                            } elseif ($keys['translator'] == $_SESSION['user']['userName'] &&
                                $keys['status'] == 'finalize') {
                                echo $echoTranslatorTaskList;
                            }
                        } elseif ($_GET['filterParam'] == 'done') {
                            if (isManager() && $keys['status'] == 'done') {
                                echo $echoManagerTaskList;
                            } elseif ($keys['translator'] == $_SESSION['user']['userName'] &&
                                $keys['status'] == 'done') {
                                echo $echoTranslatorTaskList;
                            }
                        }
                    }
                    echo "</div></form>";
                }
            }
        }
        ?>
    </div>
</div>
</div>
<?php include 'footer.php' ?>